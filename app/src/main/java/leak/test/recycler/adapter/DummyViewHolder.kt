package leak.test.recycler.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.cell_dummy.view.*
import leak.test.recycler.R

/**
 * Created by Ruslan Arslanov on 26/07/2019.
 */
class DummyViewHolder(
    itemView: View,
    private val onClickCallback: (cell: Int) -> Unit

) : RecyclerView.ViewHolder(itemView) {

    val text = itemView.context.getString(R.string.cell_dummy_text)

    fun bindCell(cell: DummyCell) {
        with(itemView) {
            textView.text = "$text ${cell.index}"
            setOnClickListener {
                onClickCallback(cell.index)
            }
        }
    }

}