package leak.test.recycler

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_next.*

/**
 * Created by Ruslan Arslanov on 26/07/2019.
 */
class NextFragment : Fragment() {

    private var fragmentSwitcher: FragmentSwitcher? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        fragmentSwitcher = context as FragmentSwitcher
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_next, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (requireActivity() as AppCompatActivity).apply {
            setHasOptionsMenu(true)
            setSupportActionBar(toolbar)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun onDetach() {
        fragmentSwitcher = null
        super.onDetach()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == android.R.id.home) {
            fragmentSwitcher?.switchToMainFragment()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }

}