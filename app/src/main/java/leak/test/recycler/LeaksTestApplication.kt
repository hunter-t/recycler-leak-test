package leak.test.recycler

import android.app.Application
import leakcanary.LeakSentry

/**
 * Created by Ruslan Arslanov on 26/07/2019.
 */
class LeaksTestApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        LeakSentry.config = LeakSentry
            .config
            .copy(
                watchFragments = true,
                watchFragmentViews = true
            )
    }
}