package leak.test.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import leak.test.recycler.adapter.DummyCell
import leak.test.recycler.adapter.DummyViewHolder

/**
 * Created by Ruslan Arslanov on 26/07/2019.
 */
class DummyAdapter(
    initiallySelectedIndex: Int,
    private val onClickCallback: (selectedIndex: Int) -> Unit

) : RecyclerView.Adapter<DummyViewHolder>() {

    private val cells = (0 until itemCount).map(::DummyCell)

    override fun getItemCount(): Int = 999

    var selectedIndex: Int = initiallySelectedIndex
        private set

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DummyViewHolder {
        val itemView = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.cell_dummy, parent, false)

        return DummyViewHolder(
            itemView = itemView,
            onClickCallback = { onClickCallback ->
                this.selectedIndex = onClickCallback
                onClickCallback(selectedIndex)
            }
        )
    }

    override fun onBindViewHolder(holder: DummyViewHolder, position: Int) {
        holder.bindCell(cells[position])
    }

}