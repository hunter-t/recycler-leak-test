package leak.test.recycler

/**
 * Created by Ruslan Arslanov on 26/07/2019.
 */
interface FragmentSwitcher {

    fun switchToNextFragment()

    fun switchToMainFragment()

}