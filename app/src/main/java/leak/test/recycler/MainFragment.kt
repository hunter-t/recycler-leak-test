package leak.test.recycler

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_main.*

/**
 * Created by Ruslan Arslanov on 26/07/2019.
 */
class MainFragment : Fragment() {

    private companion object {
        const val STATE_CELL_INDEX = "state_cell_index"
    }

    private var dummyAdapter: DummyAdapter? = null

    private var fragmentSwitcher: FragmentSwitcher? = null

    private val titleText: String by lazy {
        requireContext().getString(R.string.selected_cell)
    }

    /**
     * Use to force leak.
     * See [initAdapter].
     */
    private val titleTextUpdateHandler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {

            val activityRef = activity as? Activity
            if (activityRef == null) {
                Log.e(
                    "Leak Test MainFragment",
                    "Fragment is detached from Activity; abort."
                )
                return
            }

            val cellIndex = msg.arg1

            activityRef.runOnUiThread {
                val toolbarRef = toolbar
                if (toolbarRef != null) {
                    Log.w(
                        "Leak Test MainFragment",
                        "Successfully set Toolbar title"
                    )
                    toolbarRef.title = "$titleText #$cellIndex"
                } else {
                    Log.e(
                        "Leak Test MainFragment",
                        "Toolbar is null! Can not change its title"
                    )
                }
            }
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        fragmentSwitcher = context as FragmentSwitcher
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val initiallySelectedIndex = extractInitiallySelectedCellNumber(savedInstanceState)
        initToolbar(initiallySelectedIndex)
        initAdapter(initiallySelectedIndex)

        openNextFragmentButton.setOnClickListener {
            fragmentSwitcher?.switchToNextFragment()
        }
    }

    override fun onDestroyView() {
        recyclerView.adapter = null
        super.onDestroyView()
    }

    override fun onDetach() {
        fragmentSwitcher = null
        super.onDetach()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(STATE_CELL_INDEX, dummyAdapter?.selectedIndex ?: -1)
    }

    private fun extractInitiallySelectedCellNumber(savedInstanceState: Bundle?): Int {
        val possiblySavedIndex = dummyAdapter?.selectedIndex

        val initiallySelectedIndex = if (possiblySavedIndex != null) {
            possiblySavedIndex
        } else {
            savedInstanceState?.getInt(STATE_CELL_INDEX) ?: -1
        }

        return initiallySelectedIndex
    }

    private fun initToolbar(initiallySelectedIndex: Int) {
        (requireActivity() as AppCompatActivity).apply {
            setSupportActionBar(toolbar)
            if (initiallySelectedIndex != -1) {
                toolbar.title = "$titleText #$initiallySelectedIndex"
            }
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
        }
    }

    private fun initAdapter(initiallySelectedIndex: Int) {
        dummyAdapter = DummyAdapter(initiallySelectedIndex) { cellIndex ->
            toolbar.title = "$titleText #$cellIndex"

            // TODO: Uncomment to force leaks.
            // Thread {
            //     while (true) {
            //         Thread.sleep(3000L)
            //         val msg = Message().apply { arg1 = cellIndex }
            //         titleTextUpdateHandler.dispatchMessage(msg)
            //     }
            // }.start()
        }

        with(recyclerView) {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = dummyAdapter
        }
    }

}