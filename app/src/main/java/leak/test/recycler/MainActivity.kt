package leak.test.recycler

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction

class MainActivity : AppCompatActivity(), FragmentSwitcher {

    private companion object {
        const val TAG_MAIN_FRAGMENT = "tag_main_fragment"
        const val TAG_NEXT_FRAGMENT = "tag_next_fragment"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (supportFragmentManager.fragments.isEmpty()) {
            switchToMainFragment()
        }
    }

    override fun switchToMainFragment() {
        val ft = supportFragmentManager.beginTransaction()
        detachCurrentFragment(TAG_NEXT_FRAGMENT, ft)

        if (reattachExistingFragment(TAG_MAIN_FRAGMENT, ft).not()) {
            ft.add(R.id.fragmentContainer, MainFragment(), TAG_MAIN_FRAGMENT)
        }

        ft.commit()
    }

    override fun switchToNextFragment() {
        val ft = supportFragmentManager.beginTransaction()
        detachCurrentFragment(TAG_MAIN_FRAGMENT, ft)

        if (reattachExistingFragment(TAG_NEXT_FRAGMENT, ft).not()) {
            ft.add(R.id.fragmentContainer, NextFragment(), TAG_NEXT_FRAGMENT)
        }

        ft.commit()
    }

    private fun detachCurrentFragment(currentTag: String, ft: FragmentTransaction) {
        val currentFragment = findFragmentByTag(currentTag)
        if (currentFragment != null) {
            ft.detach(currentFragment)
        }
    }

    private fun reattachExistingFragment(tag: String, ft: FragmentTransaction): Boolean {
        val existingFragment = findFragmentByTag(tag)
        return if (existingFragment != null) {
            ft.attach(existingFragment)
            true
        } else {
            false
        }
    }

    private fun findFragmentByTag(tag: String): Fragment? {
        return supportFragmentManager.findFragmentByTag(tag)
    }

    override fun onBackPressed() {
        val topmostFragment = supportFragmentManager
            .fragments
            .getOrNull(0) as? NextFragment

        if (topmostFragment != null) {
            switchToMainFragment()
        } else {
            super.onBackPressed()
        }
    }

}
